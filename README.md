# hello-express

This project contains REST API following features:

- Welcome endpoint
- Multiply endpoint

## Getting started

First install the dependencies:
```sh
npm i
```
Start the REST API service:

`node app.js`

Or in the latest NodeJS version (>=20):

`node --watch app.js`

---
Here is a castle
![picture of a castle](castle_assignment.png)